package org.openauto.localspeedcam.modules;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;

public class AntenneAC extends TrafficModule{

    public String getFeedContent() throws IOException {
        StringBuilder builder = new StringBuilder();

        //This code has to be adapted for your local speed cam source
        Document doc = Jsoup.parse(new URL("http://www.antenne-ac.de/service/verkehr/index.html"), DEFAULT_READ_TIMEOUT);
        Element container = doc.getElementById("mainLe");
        Elements speed_cams = container.children();
        for (Element child : speed_cams) {
            builder.append(child.text());
            builder.append("\n");
        }
        if(builder.toString().isEmpty()){
            builder.append("Keine Blitzer oder Verkehrsmeldungen");
        }
        return builder.toString();

    }

    @Override
    public String getFeedTitle() {
        return "Antenne AC (Aachen)";
    }


}
